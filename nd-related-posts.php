<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
Plugin Name: ND Related Posts
Description: Provides a link to a post(s) in a chosen category and displays a post-related thumbnail 
Author: ND
Version: 1.0

*/
	
class Nd_Related_Posts extends WP_Widget {
	
	//See here for further info - https://developer.wordpress.org/reference/classes/wp_widget/__construct/
	function __construct() {
		
		parent::__construct(
			'nd-related-posts', // Base ID for CSS, i.e. #nd-related-posts
			__( 'ND Related Posts', 'text_domain' ), // The name of the widget as it appears in Available Widgets in Appearance -> Widgets
			array( 'classname'=> 'nd-related-posts', // CSS class
					'description' => __( 'Related Posts with thumbnails', 'text_domain' ), ) // The description that appears under the widget on Available Widgets
		);
	}
	
	public function widget( $args, $instance ) {
		
		$category = $instance['category'];
		$num_of_posts = $instance['num_of_posts'];		
		$show_thumbnail = $instance['show_thumbnail'];
		
		// I think this is echoing the 'before_widget' value set from the register_sidebar function. This is done by the theme but you might also have custom sidebars in functions.php
		echo $args['before_widget']; 
		
		// If the title hasn't been set
		if ( ! empty( $instance['title'] ) ) {
			
			// Echo the 'before_title' thing from register_sidebar (functions.php) then the title and then the 'after_title' thing from register_sidebar.
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
					
						
			// The custom field we use for sidebar images in posts is called 'Sidebar Image'
			//$thumbnail_custom_field = 'Sidebar Image';
			// But if you want it to be something else then create a function and pass the name of the field to this filter.
			//$thumbnail_custom_field = apply_filters( 'nd_rp_thumbnail_custom_field', $thumbnail_custom_field );
			
			// Allow values for width and height of the images to be overwritten. Declaring the values inside the apply_filters this time, just for the sake of it.
			//$thumbnail_height = apply_filters( 'nd_rp_thumbnail_height', $thumbnail_height=72 );
			//$thumbnail_width = apply_filters( 'nd_rp_thumbnail_width', $thumbnail_width=260 );
			
			// Get the list of posts of the designated category and the amount of posts we want to show
			$widget_settings = array( 'category' => $category, 'posts_per_page' => $num_of_posts );				
			$postslist = get_posts( $widget_settings );	

		?>
		
		<?php do_action( 'nd_rp_before_container' ); ?>
		
		<!-- Outer div that holds the list of posts -->
		<!-- Call the function that inserts class names for the container. Give it a default class of 'nd-related-posts-container' -->
		<div class="<?php $this->container_css_classes( 'nd-related-posts-container' ); ?>">
		
		<?php
			foreach ( $postslist as $post) :
			setup_postdata( $post ); 
			
			// Get the thumbnail info from the custom field specified above. 'True' returns a single value.
			// $thumbnail = get_post_meta( $post->ID, $thumbnail_custom_field, true );
						
			do_action( 'nd_rp_before_post' );
			?>
			
			<article>				
			<!-- Inner div that holds each individual post -->				
			<!-- Call the function that inserts class names for the individual post divs. Give it a default class of 'nd-related-posts-post' -->				
				<div class="<?php $this->post_css_classes( 'nd-related-posts-post clearfix' ); ?>">
					
					<?php 
					if ( $show_thumbnail && $show_thumbnail == '1' ) { // If the thumbnail checkbox is ticked
						
						?>
						<div class="post-thumb">
						
							<a class="nd-rp-read-more" href="<?php echo get_the_permalink( $post->ID ); ?>" title="<?php echo get_the_title( $post->ID ); ?>">
							
							<?php 
														
							if ( get_cat_name( $category ) == 'Know How' ) { 
								
								echo get_the_post_thumbnail( $post->ID, array(240, 240)); 
							}
							
							if ( get_cat_name( $category ) == 'Case Studies' ) {
								
								echo get_the_post_thumbnail( $post->ID, array(360, 80)); 
							}
							
							?>
							
							</a>
						
						</div>
						<?php						
						
					} // End checkbox check
					?>
					
					<!-- If there is no thumbnail associated with the post then apply an extra class to the post being displayed so as to remove padding from the text -->
					<div class="nd-rp-text
					
					<?php if ( !get_the_post_thumbnail( $post->ID ) ) { ?> no-thumbnail<?php } ?>">
					
						<a class="nd-rp-title" href="<?php echo get_the_permalink( $post->ID ); ?>" rel="bookmark" ><?php echo get_the_title( $post->ID ); ?></a><br>
						
						<div class="nd-rp-date"><?php echo get_the_date( 'j F Y', $post->ID ); ?></div>
						
						<div class="nd-rp-excerpt"><?php echo get_the_excerpt($post->ID); ?></div>
						
						<a class="nd-rp-read-more" href="<?php echo get_the_permalink( $post->ID ); ?>"><span class="screen-reader-text"><?php echo get_the_title( $post->ID ) ?></span>Read more...</a>
						
					</div>
					
				</div>
			
			</article>
		
		<?php
			endforeach; 
			wp_reset_postdata();
			
			do_action( 'nd_rp_after_post' );
		?>			
					
		</div> 
		<?php
		
		do_action( 'nd_rp_after_container' ); 
		
		// Now do the 'after_widget' thing from register_sidebar.
		echo $args['after_widget'];
		
	}
	
	
	// The widget form as it appears when you drag the widget into a sidebar in Appearance -> Widgets
	public function form( $instance ) {
		
		// Retrieve the title for the widget. If you haven't set a title yet then assign a blank title, e.g. ''
		// From stackexchange - "...the double underscore is a function used for translating strings based on the user's language/locale preference."
		// '?' means do the bit after the '?' if the first bit is true. If the first bit is false then do the bit after the ':' instead.
		// So, if $instance[title] is not empty then get $instance['title'], otherwise put in the empty string ''.
		
		//$title = ! empty( $instance['title'] ) ? $instance['title'] : __( '', 'text_domain' );
		
		$title = $instance['title'];
		$category = $instance['category'];
		$num_of_posts = $instance['num_of_posts'];
		$show_thumbnail = $instance['show_thumbnail'];			
		
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" 
					name="<?php echo $this->get_field_name( 'title' ); ?>" 
					type="text" value="<?php echo esc_attr( $title ); ?>"
			/>
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Category:' ); // _e() translates the text ?></label>
		
		<!-- Not entirely sure what get_field_name('category') is all about, but it outputs name="widget-nd-related-posts[6][category]" 6 changes depending on how many other version of the same widget you've used and [category] is generated by the 'category' in the brackets of get_field_name(). I think it needs to be 'category' to match $instance['category'].
		'selected' => $category retreives the current category from the declarations above i.e. $category = $instance['category'];
		The 'id' bit creates id="widget-nd-related-posts-6-category". ('category') is appeanded to widget-nd-related-posts-6-
		Hierarchical just lists the cateogories in parent/child format in the dropdown to make it look better    -->
		
		<?php wp_dropdown_categories( array('name' => $this->get_field_name('category'), 
											'selected' => $category, 
											'id' => $this->get_field_id('category'), 
											'class' => 'widefat', 
											'hierarchical'=>'1')); ?>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'num_of_posts' ); ?>"><?php _e( 'Number of Posts to show:' ); ?></label><br>
			<input id="<?php echo $this->get_field_id( 'num_of_posts' ); ?>" 
					name="<?php echo $this->get_field_name( 'num_of_posts' ); ?>" 
					type="text" size="2" value="<?php echo esc_attr( $num_of_posts ); ?>" />
		</p>
		
		<p>
			<input id="<?php echo $this->get_field_id('show_thumbnail'); ?>" name="<?php echo $this->get_field_name('show_thumbnail'); ?>" type="checkbox" value="1" <?php checked( '1', $show_thumbnail ); ?> />
			<label for="<?php echo $this->get_field_id('show_thumbnail'); ?>"><?php _e('Show thumbnail', 'nd-related-posts'); ?></label>	
		</p>
		
		<?php 
	}
	
	
	public function update( $new_instance, $old_instance ) {
		
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['category'] = ( ! empty( $new_instance['category'] ) ) ? $new_instance['category'] : '';
		$instance['num_of_posts'] = ( ! empty( $new_instance['num_of_posts'] ) ) ? strip_tags( $new_instance['num_of_posts'] ) : '';
		$instance['show_thumbnail'] = ( $new_instance['show_thumbnail'] );
		
		return $instance;
	}
	
	private function container_css_classes( $classes ) {

		// In order to add classes to the class above, the function you create should say $classes .= ' your-class-1 your-class-2';
		// $class = 'your-class-1 your-class-2'; will overwrite the class name above.
		$classes = apply_filters( 'nd_rp_add_more_container_classes', $classes );
		
		echo esc_attr( $classes );
	}

	// Add the classes to the div around each post
	private function post_css_classes( $classes ) {	
		
		// In order to add classes to the class above the function you create should say $classes .= ' your-class-1 your-class-2';
		// $class = 'your-class-1 your-class-2'; will overwrite the class name above.
		$classes = apply_filters( 'nd_rp_add_more_post_classes', $classes );
		
		echo esc_attr( $classes );
	}


}

// register widget
function register_ndrelatedposts() {
    
	register_widget( 'Nd_Related_Posts' );
}
add_action( 'widgets_init', 'register_ndrelatedposts' );
